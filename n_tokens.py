
def estimate_price(num_of_episodes=3, in_context_len=840, ep_obs_len=25, n_steps=10, model="text-davinci-003", feed_episode_history=False):
    if model == "text-ada-001":
        # ada
        price_per_1k = 0.0004
    elif model == "text-davinci-003":
        # davinci
        # davinci
        price_per_1k = 0.002
    elif model in ["bloom", "dummy", "gpt2", "gpt2_large", "bloom_560m", "gpt_j", "random", "api_bloom"]:
        price_per_1k = 0

    if feed_episode_history:
        total_tokens = num_of_episodes*(n_steps*in_context_len + ep_obs_len*sum(range(n_steps)))
    else:
        total_tokens = num_of_episodes*(in_context_len + n_steps*ep_obs_len)
    price = (total_tokens/1000)*price_per_1k
    return total_tokens, price


if __name__ == "__main__":
    total_tokens, price = estimate_price()
    print("tokens:", total_tokens)
    print("price:", price)