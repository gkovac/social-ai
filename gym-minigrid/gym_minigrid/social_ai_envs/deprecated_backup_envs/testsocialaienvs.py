from gym_minigrid.social_ai_envs.socialaiparamenv import SocialAIParamEnv
from gym_minigrid.parametric_env import *
from gym_minigrid.register import register

import inspect, importlib

# for used for automatic registration of environments
defined_classes = [name for name, _ in inspect.getmembers(importlib.import_module(__name__), inspect.isclass)]

# InformationSeeking


# Boxes
class TestLanguageColorBoxesInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Boxes", parent=problem_nd, type="value")

        return tree


class TestLanguageFeedbackBoxesInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Boxes", parent=problem_nd, type="value")

        return tree


class TestPointingBoxesInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Boxes", parent=problem_nd, type="value")

        return tree


class TestEmulationBoxesInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Boxes", parent=problem_nd, type="value")

        return tree


# Levers
class TestLanguageColorLeversInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Levers", parent=problem_nd, type="value")

        return tree


class TestLanguageFeedbackLeversInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Levers", parent=problem_nd, type="value")

        return tree


class TestPointingLeversInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Levers", parent=problem_nd, type="value")

        return tree


class TestEmulationLeversInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Levers", parent=problem_nd, type="value")

        return tree


# Doors
class TestLanguageColorDoorsInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Doors", parent=problem_nd, type="value")

        return tree


class TestLanguageFeedbackDoorsInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Doors", parent=problem_nd, type="value")

        return tree


class TestPointingDoorsInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Doors", parent=problem_nd, type="value")

        return tree


class TestEmulationDoorsInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Doors", parent=problem_nd, type="value")

        return tree


# Switches
class TestLanguageColorSwitchesInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Switches", parent=problem_nd, type="value")

        return tree


class TestLanguageFeedbackSwitchesInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Switches", parent=problem_nd, type="value")

        return tree


class TestPointingSwitchesInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Switches", parent=problem_nd, type="value")

        return tree


class TestEmulationSwitchesInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Switches", parent=problem_nd, type="value")

        return tree


# Marble
class TestLanguageColorMarbleInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Marble", parent=problem_nd, type="value")

        return tree


class TestLanguageFeedbackMarbleInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Marble", parent=problem_nd, type="value")

        return tree


class TestPointingMarbleInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Marble", parent=problem_nd, type="value")

        return tree


class TestEmulationMarbleInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Marble", parent=problem_nd, type="value")

        return tree


# Generators
class TestLanguageColorGeneratorsInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Generators", parent=problem_nd, type="value")

        return tree


class TestLanguageFeedbackGeneratorsInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Generators", parent=problem_nd, type="value")

        return tree


class TestPointingGeneratorsInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Generators", parent=problem_nd, type="value")

        return tree


class TestEmulationGeneratorsInformationSeekingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")

        N_bo_nd = tree.add_node("N", parent=inf_seeking_nd, type="param")
        tree.add_node("2", parent=N_bo_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
        tree.add_node("Generators", parent=problem_nd, type="value")

        return tree


# Collaboration
class TestLeverDoorCollaborationEnv(SocialAIParamEnv):
    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Collaboration
        collab_nd = tree.add_node("Collaboration", parent=env_type_nd, type="value")

        colab_type_nd = tree.add_node("Problem", parent=collab_nd, type="param")
        tree.add_node("LeverDoor", parent=colab_type_nd, type="value")

        role_nd = tree.add_node("Version", parent=collab_nd, type="param")
        tree.add_node("Social", parent=role_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=collab_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree


class TestMarblePushCollaborationEnv(SocialAIParamEnv):
    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Collaboration
        collab_nd = tree.add_node("Collaboration", parent=env_type_nd, type="value")

        colab_type_nd = tree.add_node("Problem", parent=collab_nd, type="param")
        tree.add_node("MarblePush", parent=colab_type_nd, type="value")

        role_nd = tree.add_node("Version", parent=collab_nd, type="param")
        tree.add_node("Social", parent=role_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=collab_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree


class TestMarblePassCollaborationEnv(SocialAIParamEnv):
    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Collaboration
        collab_nd = tree.add_node("Collaboration", parent=env_type_nd, type="value")

        colab_type_nd = tree.add_node("Problem", parent=collab_nd, type="param")
        tree.add_node("MarblePass", parent=colab_type_nd, type="value")

        role_nd = tree.add_node("Version", parent=collab_nd, type="param")
        tree.add_node("Social", parent=role_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=collab_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree


# Perspective Taking
class TestAppleStealingPerspectiveTakingEnv(SocialAIParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Collaboration
        collab_nd = tree.add_node("OthersPerceptionInference", parent=env_type_nd, type="value")

        colab_type_nd = tree.add_node("Problem", parent=collab_nd, type="param")
        tree.add_node("AppleStealing", parent=colab_type_nd, type="value")
        role_nd = tree.add_node("Version", parent=collab_nd, type="param")
        social_nd = tree.add_node("Social", parent=role_nd, type="value")

        tree.add_node("Asocial", parent=role_nd, type="value")
        social_nd = tree.add_node("Social", parent=role_nd, type="value")

        role_nd = tree.add_node("NPC_movement", parent=social_nd, type="param")
        tree.add_node("Walking", parent=role_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=collab_nd, type="param")
        tree.add_node("A_lot", parent=obstacles_nd, type="value")

        return tree

# automatic registration of environments
defined_classes_ = [name for name, _ in inspect.getmembers(importlib.import_module(__name__), inspect.isclass)]

envs = list(set(defined_classes_) - set(defined_classes))
assert all([e.endswith("Env") for e in envs])

for env in envs:
    register(
        id='SocialAI-{}-v1'.format(env),
        entry_point='gym_minigrid.social_ai_envs:{}'.format(env)
    )
