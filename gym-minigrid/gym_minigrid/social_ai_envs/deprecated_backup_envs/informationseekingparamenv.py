from itertools import chain
from gym_minigrid.minigrid import *
from gym_minigrid.parametric_env import *
from gym_minigrid.register import register
from gym_minigrid.social_ai_envs import InformationSeekingEnv
from gym_minigrid.social_ai_envs.socialaigrammar import SocialAIGrammar, SocialAIActions, SocialAIActionSpace
from gym_minigrid.curriculums import *

import inspect, importlib

# for used for automatic registration of environments
defined_classes = [name for name, _ in inspect.getmembers(importlib.import_module(__name__), inspect.isclass)]
raise DeprecationWarning("Use SocialAIParamEnv")


class InformationSeekingParamEnv(gym.Env):
    """
    Meta-Environment containing all other environment (multi-task learning)
    """

    def __init__(
            self,
            size=10,
            # size=15,
            hidden_npc=False,
            see_through_walls=False,
            is_test_env=False,
            max_steps=80,  # before it was 50, 80 is maybe better because of emulation ?
            switch_no_light=True,  # v1 change
            curriculum=None,
            expert_curriculum_thresholds=(0.9, 0.8),
            expert_curriculum_average_interval=100,
            expert_curriculum_minimum_episodes=1000,
    ):

        print(f"See through walls: {see_through_walls}")

        # Number of cells (width and height) in the agent view
        self.agent_view_size = 7

        # Number of object dimensions (i.e. number of channels in symbolic image)
        self.encoding_size = 8

        self.max_steps = max_steps

        self.switch_no_light = switch_no_light

        # Observations are dictionaries containing an
        # encoding of the grid and a textual 'mission' string
        self.observation_space = spaces.Box(
            low=0,
            high=255,
            shape=(self.agent_view_size, self.agent_view_size, self.encoding_size),
            dtype='uint8'
        )
        self.observation_space = spaces.Dict({
            'image': self.observation_space
        })

        self.hidden_npc = hidden_npc  # TODO: implement hidden npc

        # construct the tree
        self.parameter_tree = self.construct_tree()

        # print tree for logging purposes
        self.parameter_tree.print_tree()

        if curriculum == "scaffolding_expert":
            print("Scaffolding Expert")
            self.expert_curriculum_thresholds = expert_curriculum_thresholds
            self.expert_curriculum_average_interval = expert_curriculum_average_interval
            self.expert_curriculum_minimum_episodes = expert_curriculum_minimum_episodes
            self.curriculum = ScaffoldingExpertCurriculum(
                phase_thresholds=self.expert_curriculum_thresholds,
                average_interval=self.expert_curriculum_average_interval,
                minimum_episodes=self.expert_curriculum_minimum_episodes,
            )
        else:
            self.curriculum = curriculum

        self.current_env = None

        # setup grammar
        self.env_list = [InformationSeekingEnv(
            max_steps=max_steps,
            size=size,
            switch_no_light=self.switch_no_light,
            see_through_walls=see_through_walls,
        )]

        self.all_npc_utterance_actions = sorted(list(set(chain(*[e.all_npc_utterance_actions for e in self.env_list]))))

        # instantiate envs
        self.envs = {
            "Info": self.env_list[0],
        }

        self.grammar = SocialAIGrammar()

        # set up the action space
        self.action_space = SocialAIActionSpace
        self.actions = SocialAIActions
        self.npc_prim_actions_dict = SocialAINPCActionsDict

        # all envs must have the same grammar
        for env in self.env_list:
            assert isinstance(env.grammar, type(self.grammar))
            assert env.actions is self.actions
            assert env.action_space is self.action_space

            # suggestion: encoding size is automatically set to max?
            assert env.encoding_size is self.encoding_size
            assert env.observation_space == self.observation_space
            assert env.prim_actions_dict == self.npc_prim_actions_dict

        self.reset()

    def draw_tree(self, ignore_labels=[], save_dir="viz"):
        self.parameter_tree.draw_tree("{}/param_tree_{}".format(save_dir, self.__class__.__name__), ignore_labels=ignore_labels)

    def print_tree(self):
        self.parameter_tree.print_tree()

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Apples
        tree.add_node("Apples", parent=problem_nd, type="value")

        # Boxes
        boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        tree.add_node("1", parent=N_bo_nd, type="value")
        tree.add_node("2", parent=N_bo_nd, type="value")

        # n colors boxes
        Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_bo_nd, type="value")

        # Switches
        switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        tree.add_node("1", parent=N_sw_nd, type="value")
        tree.add_node("2", parent=N_sw_nd, type="value")

        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # Marbles
        marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        tree.add_node("1", parent=N_mar_nd, type="value")
        tree.add_node("2", parent=N_mar_nd, type="value")

        # n colors marbles
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # Generators
        generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        tree.add_node("1", parent=N_gen_nd, type="value")
        tree.add_node("2", parent=N_gen_nd, type="value")

        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree

    def construct_env_from_params(self, params):
        params_labels = {k.label: v.label for k, v in params.items()}
        if params_labels['Env_type'] == "Collaboration":

            if params_labels["Role"] == "A":
                env = self.envs["ButtonDoor-A"]

            elif params_labels["Role"] == "B":
                env = self.envs["ButtonDoor-B"]

            else:
                raise ValueError("params badly defined.")

        elif params_labels['Env_type'] == "Information_seeking":
            env = self.envs["Info"]

        else:
            raise ValueError("params badly defined.")

        reset_kwargs = params_labels

        return env, reset_kwargs

    def reset(self):
        # select a new social environment at random, for each new episode

        # todo: see if you can make this window transfer simpler
        old_window = None
        if self.current_env:  # a previous env exists, save old window
            old_window = self.current_env.window

        # # sample new environment
        # self.current_env = np.random.choice(self.env_list)

        self.current_params = self.parameter_tree.sample_env_params(ACL=self.curriculum)
        # print("param:", [(k.label,v.label) for k,v in params.items()])
        self.current_env, reset_kwargs = self.construct_env_from_params(self.current_params)
        assert reset_kwargs is not {}
        assert reset_kwargs is not None

        obs = self.current_env.reset(**reset_kwargs)

        # carry on window if this env is not the first
        if old_window:
            self.current_env.window = old_window
        return obs

    def seed(self, seed=1337):
        # Seed the random number generator
        for env in self.env_list:
            env.seed(seed)
        return [seed]

    def set_curriculum_parameters(self, params):
        if self.curriculum is not None:
            self.curriculum.set_parameters(params)
            # assert {k.label: v.label for k, v in self.current_params.items()} == self.current_env.parameters
            # self.curriculum.update(self.current_params, obs, reward, done, info)

    def step(self, action):
        assert self.current_env
        assert self.current_env.parameters is not None

        obs, reward, done, info = self.current_env.step(action)

        # v1 change: reward is diminished less that normal before,
        # the underlying env just has to provide a positive reward
        if reward > 0:
            assert reward == self._reward()

        info["parameters"] = self.current_params  # todo: add to other param envs

        if done:
            if info["success"]:
                self.current_env.outcome_info = "SUCCESS: agent got {} reward \n".format(np.round(reward, 1))
            else:
                self.current_env.outcome_info = "FAILURE: agent got {} reward \n".format(reward)

        if self.curriculum is not None:
            for k, v in self.curriculum.get_info().items():
                info["curriculum_info_"+k] = v

        return obs, reward, done, info

    def _reward(self):
        # low diminish
        return 1 - 0.1 * (self.step_count / self.max_steps)

    @property
    def window(self):
        assert self.current_env
        return self.current_env.window

    @window.setter
    def window(self, value):
        self.current_env.window = value

    def render(self, *args, **kwargs):
        assert self.current_env
        return self.current_env.render(*args, **kwargs)

    @property
    def step_count(self):
        return self.current_env.step_count

    def get_mission(self):
        return self.current_env.get_mission()

class AskEyeContactPointingBoxesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")
        # tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Boxes
        boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # tree.add_node("1", parent=N_bo_nd, type="value")
        tree.add_node("2", parent=N_bo_nd, type="value")

        # n colors boxes
        Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")
        #
        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree

class AskEyeContactLanguageBoxesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        # tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Boxes
        boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # tree.add_node("1", parent=N_bo_nd, type="value")
        tree.add_node("2", parent=N_bo_nd, type="value")

        # n colors boxes
        Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        for i in range(2, len(COLOR_NAMES)+1):
            tree.add_node("{}".format(i), parent=Num_of_colors_bo_nd, type="value")

        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")
        #
        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree

class AskEyeContactLanguageSwitchesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        # tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_bo_nd, type="value")
        #
        # Switches
        switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # tree.add_node("1", parent=N_sw_nd, type="value")
        tree.add_node("2", parent=N_sw_nd, type="value")

        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree

class AskEyeContactEmulationMarbleInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Polite", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")
        # tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_bo_nd, type="value")
        #
        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # Marbles
        marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # tree.add_node("1", parent=N_mar_nd, type="value")
        tree.add_node("2", parent=N_mar_nd, type="value")

        # n colors marbles
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree

class AskEyeContactEmulationGeneratorsInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")
        # tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_bo_nd, type="value")
        #
        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # Generators
        generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # tree.add_node("1", parent=N_gen_nd, type="value")
        tree.add_node("2", parent=N_gen_nd, type="value")
        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree

# class AskEyeContactEmulationBoxesInformationSeekingParamEnv(InformationSeekingParamEnv):
#
#     def construct_tree(self):
#         tree = ParameterTree()
#
#         env_type_nd = tree.add_node("Env_type", type="param")
#
#         # Information seeking
#         inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")
#
#         prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
#         # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
#         # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
#         # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
#         tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")
#
#         # scaffolding
#         scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
#         # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
#         scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")
#
#         emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
#         tree.add_node("Y", parent=emulation_nd, type="value")
#         # tree.add_node("N", parent=emulation_nd, type="value")
#
#         pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
#         # tree.add_node("Direct", parent=pointing_nd, type="value")
#         tree.add_node("No", parent=pointing_nd, type="value")
#
#         language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
#         # tree.add_node("Color", parent=language_grounding_nd, type="value")
#         # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
#         tree.add_node("No", parent=language_grounding_nd, type="value")
#
#         problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")
#
#         # Boxes
#         boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
#         N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
#         # tree.add_node("1", parent=N_bo_nd, type="value")
#         tree.add_node("2", parent=N_bo_nd, type="value")
#
#         # n colors boxes
#         Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
#         for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_bo_nd, type="value")
#         #
#         # # Switches
#         # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
#         # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
#         # # tree.add_node("1", parent=N_sw_nd, type="value")
#         # tree.add_node("2", parent=N_sw_nd, type="value")
#         #
#         # # n colors switches
#         # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
#         # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")
#
#         # Marbles
#         # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
#         # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
#         # # tree.add_node("1", parent=N_mar_nd, type="value")
#         # tree.add_node("2", parent=N_mar_nd, type="value")
#         #
#         # # n colors marbles
#         # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
#         # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")
#
#         # # Generators
#         # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
#         # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
#         # # tree.add_node("1", parent=N_gen_nd, type="value")
#         # tree.add_node("2", parent=N_gen_nd, type="value")
#         #
#         # # n colors switches
#         # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
#         # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")
#
#         obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
#         tree.add_node("No", parent=obstacles_nd, type="value")
#
#         return tree
#
class AskPointingBoxesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")
        # tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        tree.add_node("2", parent=N_bo_nd, type="value")
        # n colors boxes
        Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_bo_nd, type="value")

        #
        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")

        # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree

class NoIntroPointingBoxesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")
        # tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        tree.add_node("2", parent=N_bo_nd, type="value")
        # n colors boxes
        Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_bo_nd, type="value")

        #
        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")

        # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree


# testing set # todo: move to another file

# Boxes
class TestLanguageColorBoxesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        # tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Boxes
        boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # tree.add_node("1", parent=N_bo_nd, type="value")
        tree.add_node("2", parent=N_bo_nd, type="value")

        # n colors boxes
        Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")
        #
        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")

        return tree

class TestLanguageFeedbackBoxesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        # tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Boxes
        boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # tree.add_node("1", parent=N_bo_nd, type="value")
        tree.add_node("2", parent=N_bo_nd, type="value")

        # n colors boxes
        Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")
        #
        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestPointingBoxesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")
        # tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Boxes
        boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # tree.add_node("1", parent=N_bo_nd, type="value")
        tree.add_node("2", parent=N_bo_nd, type="value")

        # n colors boxes
        Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")
        #
        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestEmulationBoxesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")
        # tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Boxes
        boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # tree.add_node("1", parent=N_bo_nd, type="value")
        tree.add_node("2", parent=N_bo_nd, type="value")

        # n colors boxes
        Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # for i in range(2, len(COLOR_NAMES)+1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")
        #
        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree


# Switches
class TestLanguageColorSwitchesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        # tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # Switches
        switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # tree.add_node("1", parent=N_sw_nd, type="value")
        tree.add_node("2", parent=N_sw_nd, type="value")

        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestLanguageFeedbackSwitchesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        # tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # Switches
        switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # tree.add_node("1", parent=N_sw_nd, type="value")
        tree.add_node("2", parent=N_sw_nd, type="value")

        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestPointingSwitchesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")
        # tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # Switches
        switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # tree.add_node("1", parent=N_sw_nd, type="value")
        tree.add_node("2", parent=N_sw_nd, type="value")

        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestEmulationSwitchesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")
        # tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # Switches
        switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # tree.add_node("1", parent=N_sw_nd, type="value")
        tree.add_node("2", parent=N_sw_nd, type="value")

        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # # Marbles
        # marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        # N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # # tree.add_node("1", parent=N_mar_nd, type="value")
        # tree.add_node("2", parent=N_mar_nd, type="value")
        #
        # # n colors marbles
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

# Marbles
class TestLanguageColorMarblesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        # tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")
        #
        # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # Marbles
        marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # tree.add_node("1", parent=N_mar_nd, type="value")
        tree.add_node("2", parent=N_mar_nd, type="value")

        # n colors marbles
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestLanguageFeedbackMarblesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        # tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # Marbles
        marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # tree.add_node("1", parent=N_mar_nd, type="value")
        tree.add_node("2", parent=N_mar_nd, type="value")

        # n colors marbles
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestPointingMarblesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")
        # tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")
        #
        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # Marbles
        marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # tree.add_node("1", parent=N_mar_nd, type="value")
        tree.add_node("2", parent=N_mar_nd, type="value")

        # n colors marbles
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestEmulationMarblesInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")
        # tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # # Boxes
        # boxes_nd = tree.add_node("Boxes", parent=problem_nd, type="value")
        # N_bo_nd = tree.add_node("N", parent=boxes_nd, type="param")
        # # tree.add_node("1", parent=N_bo_nd, type="value")
        # tree.add_node("2", parent=N_bo_nd, type="value")
        #
        # # n colors boxes
        # Num_of_colors_bo_nd = tree.add_node("Num_of_colors", parent=boxes_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_bo_nd, type="value")

        # # Switches
        # switches_nd = tree.add_node("Switches", parent=problem_nd, type="value")
        # N_sw_nd = tree.add_node("N", parent=switches_nd, type="param")
        # # tree.add_node("1", parent=N_sw_nd, type="value")
        # tree.add_node("2", parent=N_sw_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=switches_nd, type="param")
        # tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # Marbles
        marbles_nd = tree.add_node("Marbles", parent=problem_nd, type="value")
        N_mar_nd = tree.add_node("N", parent=marbles_nd, type="param")
        # tree.add_node("1", parent=N_mar_nd, type="value")
        tree.add_node("2", parent=N_mar_nd, type="value")

        # n colors marbles
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=marbles_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        # # Generators
        # generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        # N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # # tree.add_node("1", parent=N_gen_nd, type="value")
        # tree.add_node("2", parent=N_gen_nd, type="value")
        #
        # # n colors switches
        # Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        # for i in range(2, len(COLOR_NAMES) + 1): tree.add_node("{}".format(i), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree


# Generators
class TestLanguageColorGeneratorsInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        # tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Generators
        generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # tree.add_node("1", parent=N_gen_nd, type="value")
        tree.add_node("2", parent=N_gen_nd, type="value")

        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestLanguageFeedbackGeneratorsInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        # tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Generators
        generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # tree.add_node("1", parent=N_gen_nd, type="value")
        tree.add_node("2", parent=N_gen_nd, type="value")
        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestPointingGeneratorsInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Y", parent=emulation_nd, type="value")
        tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        tree.add_node("Direct", parent=pointing_nd, type="value")
        # tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Generators
        generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # tree.add_node("1", parent=N_gen_nd, type="value")
        tree.add_node("2", parent=N_gen_nd, type="value")
        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree

class TestEmulationGeneratorsInformationSeekingParamEnv(InformationSeekingParamEnv):

    def construct_tree(self):
        tree = ParameterTree()

        env_type_nd = tree.add_node("Env_type", type="param")

        # Information seeking
        inf_seeking_nd = tree.add_node("Information_seeking", parent=env_type_nd, type="value")

        prag_fr_compl_nd = tree.add_node("Pragmatic_frame_complexity", parent=inf_seeking_nd, type="param")
        # tree.add_node("No", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Eye_contact", parent=prag_fr_compl_nd, type="value")
        # tree.add_node("Ask", parent=prag_fr_compl_nd, type="value")
        tree.add_node("Ask_Eye_contact", parent=prag_fr_compl_nd, type="value")

        # scaffolding
        scaffolding_nd = tree.add_node("Scaffolding", parent=inf_seeking_nd, type="param")
        # scaffolding_Y_nd = tree.add_node("Y", parent=scaffolding_nd, type="value")
        scaffolding_N_nd = tree.add_node("N", parent=scaffolding_nd, type="value")

        emulation_nd = tree.add_node("Emulation", parent=scaffolding_N_nd, type="param")
        tree.add_node("Y", parent=emulation_nd, type="value")
        # tree.add_node("N", parent=emulation_nd, type="value")

        pointing_nd = tree.add_node("Pointing", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Direct", parent=pointing_nd, type="value")
        tree.add_node("No", parent=pointing_nd, type="value")

        language_grounding_nd = tree.add_node("Language_grounding", parent=scaffolding_N_nd, type="param")
        # tree.add_node("Color", parent=language_grounding_nd, type="value")
        # tree.add_node("Feedback", parent=language_grounding_nd, type="value")
        tree.add_node("No", parent=language_grounding_nd, type="value")

        problem_nd = tree.add_node("Problem", parent=inf_seeking_nd, type="param")

        # Generators
        generators_nd = tree.add_node("Generators", parent=problem_nd, type="value")
        N_gen_nd = tree.add_node("N", parent=generators_nd, type="param")
        # tree.add_node("1", parent=N_gen_nd, type="value")
        tree.add_node("2", parent=N_gen_nd, type="value")
        # n colors switches
        Num_of_colors_sw_nd = tree.add_node("Num_of_colors", parent=generators_nd, type="param")
        tree.add_node("{}".format(len(COLOR_NAMES)), parent=Num_of_colors_sw_nd, type="value")

        obstacles_nd = tree.add_node("Obstacles", parent=inf_seeking_nd, type="param")
        tree.add_node("No", parent=obstacles_nd, type="value")  # todo: should we use obstacles in testing?

        return tree



# automatic registration of environments

defined_classes_ = [name for name, _ in inspect.getmembers(importlib.import_module(__name__), inspect.isclass)]

envs = list(set(defined_classes_) - set(defined_classes))
assert all([e.endswith("Env") for e in envs])

for env in envs:
    register(
        id='SocialAI-{}-v1'.format(env),
        entry_point='gym_minigrid.social_ai_envs:{}'.format(env)
    )
