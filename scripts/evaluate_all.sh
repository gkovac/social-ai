#
# TalkItOut
#python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label TalkItOut_bonus --model-to-evaluate ./storage/20-05_NeurIPS_TalkItOutPolite_env_MiniGrid-TalkItOutPolite-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_lang_exploration-bonus-params_7_50_exploration-bonus-tanh_0.6/ &
#python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label TalkItOut_no_bonus --model-to-evaluate ./storage/20-05_NeurIPS_TalkItOutPolite_NO_BONUS_env_MiniGrid-TalkItOutPolite-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
#python -m scripts.evaluate_new --episodes 500 -q  --test-set-seed 1  --model-label TalkItOut_no_social  --env_args hidden_npc True --model-to-evaluate ./storage/26-05_NeurIPS_gpu_TalkItOutPolite_NoSocial_NO_BONUS_env_MiniGrid-TalkItOutPolite-8x8-v0_env_args_hidden_npc_True_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
#wait
#
# ablation
#python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label TalkItOutNoNPC_bonus --model-to-evaluate ./storage/20-05_NeurIPS_TalkItOutPolite_NoLiar_env_MiniGrid-TalkItOutNoLiarPolite-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_lang_exploration-bonus-params_7_50_exploration-bonus-tanh_0.6/ &
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label TalkItOutNoNPC_no_bonus --model-to-evaluate ./storage/20-05_NeurIPS_TalkItOutPolite_NO_BONUS_NoLiar_env_MiniGrid-TalkItOutNoLiarPolite-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
python -m scripts.evaluate_new --episodes 500 -q  --test-set-seed 1  --model-label TalkItOutNoNPC_no_social  --env_args hidden_npc True --model-to-evaluate ./storage/26-05_NeurIPS_gpu_TalkItOutPolite_NoSocial_NO_BONUS_NoLiar_env_MiniGrid-TalkItOutNoLiarPolite-8x8-v0_env_args_hidden_npc_True_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
wait
#
#
# DiverseExit
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label DiverseExit_bonus --model-to-evaluate ./storage/24-05_NeurIPS_DiverseExit_BONUS_env_MiniGrid-DiverseExit-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_lang_exploration-bonus-params_20_50_exploration-bonus-tanh_0.6/ &
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label DiverseExit_no_bonus --model-to-evaluate ./storage/24-05_NeurIPS_DiverseExit_NO_BONUS_env_MiniGrid-DiverseExit-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
python -m scripts.evaluate_new --episodes 500  --test-set-seed 1  --model-label DiverseExit_no_social  --env_args hidden_npc True --model-to-evaluate ./storage/26-05_NeurIPS_gpu_DiverseExit_NoSocial_NO_BONUS_env_MiniGrid-DiverseExit-8x8-v0_env_args_hidden_npc_True_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
wait
#
# ShowMe
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label ShowMe_bonus --eval-env MiniGrid-ShowMe-8x8-v0 --model-to-evaluate ./storage/20-05_NeurIPS_ShowMe_CEB_env_MiniGrid-DemonstrationNoLightNoTurnOff100-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_cell_exploration-bonus-params_3_50_exploration-bonus-tanh_0.6/ &
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label ShowMe_no_bonus --eval-env MiniGrid-ShowMe-8x8-v0 --model-to-evaluate ./storage/20-05_NeurIPS_ShowMe_NO_BONUS_env_MiniGrid-DemonstrationNoLightNoTurnOff100-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label ShowMe_no_social --eval-env MiniGrid-ShowMe-8x8-v0 --env_args hidden_npc True --model-to-evaluate ./storage/26-05_NeurIPS_gpu_ShowMe_NoSocial_NO_BONUS_env_MiniGrid-ShowMe-8x8-v0_env_args_hidden_npc_True_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
wait
#
# HELP
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label HELP_bonus --eval-env MiniGrid-Helper-8x8-v0  --model-to-evaluate ./storage/24-05_NeurIPS_Help_BONUS_env_MiniGrid-Exiter-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_cell_exploration-bonus-params_3_50_exploration-bonus-tanh_0.6/ &
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label HELP_no_bonus --eval-env MiniGrid-Helper-8x8-v0  --model-to-evaluate ./storage/24-05_NeurIPS_Help_NO_BONUS_env_MiniGrid-Exiter-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label HELP_no_social --eval-env MiniGrid-Helper-8x8-v0  --env_args hidden_npc True --model-to-evaluate ./storage/26-05_NeurIPS_gpu_Help_NoSocial_NO_BONUS_env_MiniGrid-Exiter-8x8-v0_env_args_hidden_npc_True_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
wait
#
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label HELP_training_bonus --eval-env MiniGrid-Exiter-8x8-v0  --model-to-evaluate ./storage/24-05_NeurIPS_Help_BONUS_env_MiniGrid-Exiter-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_cell_exploration-bonus-params_3_50_exploration-bonus-tanh_0.6/ &
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label HELP_training_no_bonus --eval-env MiniGrid-Exiter-8x8-v0  --model-to-evaluate ./storage/24-05_NeurIPS_Help_NO_BONUS_env_MiniGrid-Exiter-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
python -m scripts.evaluate_new --episodes 500 -q --test-set-seed 1  --model-label HELP_training_no_social --eval-env MiniGrid-Exiter-8x8-v0  --env_args hidden_npc True --model-to-evaluate ./storage/26-05_NeurIPS_gpu_Help_NoSocial_NO_BONUS_env_MiniGrid-Exiter-8x8-v0_env_args_hidden_npc_True_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2/ &
wait
