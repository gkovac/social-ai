#
#python3 -m scripts.visualize --model storage/19-05_coinThief_env_args_few_actions_True_tag_visible_coins_True_npc_view_size_5_npc_look_around_True/1/ --dialogue --mp4 model_id --pause 1.0 --seed $RANDOM --episodes 100

# TalkItOut
python3 -m scripts.visualize --model \
storage/20-05_NeurIPS_TalkItOutPolite_env_MiniGrid-TalkItOutPolite-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_lang_exploration-bonus-params_7_50_exploration-bonus-tanh_0.6/0 \
--dialogue --mp4 graphics/neurips/neurips_TalkItOut_bonus --pause 0.5 --seed $RANDOM --episodes 10 &
# ablation
python3 -m scripts.visualize --model \
./storage/20-05_NeurIPS_TalkItOutPolite_NoLiar_env_MiniGrid-TalkItOutNoLiarPolite-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_lang_exploration-bonus-params_7_50_exploration-bonus-tanh_0.6/0 \
--dialogue --mp4 graphics/neurips/neurips_TalkItOut_NoLiar_bonus --pause 0.5 --seed $RANDOM --episodes 10 &
#
# DiverseExit
python3 -m scripts.visualize --model \
./storage/24-05_NeurIPS_DiverseExit_BONUS_env_MiniGrid-DiverseExit-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_lang_exploration-bonus-params_20_50_exploration-bonus-tanh_0.6/0 \
--dialogue --mp4 graphics/neurips/neurips_DiverseExit_bonus --pause 0.5 --seed $RANDOM --episodes 10 &
# ShowMe
wait
python3 -m scripts.visualize --model \
./storage/20-05_NeurIPS_ShowMe_CEB_env_MiniGrid-DemonstrationNoLightNoTurnOff100-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_cell_exploration-bonus-params_3_50_exploration-bonus-tanh_0.6/0 \
--dialogue --mp4 graphics/neurips/neurips_ShowMe_bonus --pause 0.5 --seed $RANDOM --episodes 10 --env-name MiniGrid-ShowMe-8x8-v0 &
#
# Help
python3 -m scripts.visualize --model \
./storage/24-05_NeurIPS_Help_BONUS_env_MiniGrid-Exiter-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_cell_exploration-bonus-params_3_50_exploration-bonus-tanh_0.6/0 \
--dialogue --mp4 graphics/neurips/neurips_help_train_bonus --pause 0.5 --seed $RANDOM --episodes 10 &
# Help - eval
python3 -m scripts.visualize --model \
./storage/24-05_NeurIPS_Help_BONUS_env_MiniGrid-Exiter-8x8-v0_multi-modal-babyai11-agent_arch_original_endpool_res_custom-ppo-2_exploration-bonus-type_cell_exploration-bonus-params_3_50_exploration-bonus-tanh_0.6/0 \
--dialogue --mp4 graphics/neurips/neurips_help_eval_bonus --pause 0.5 --seed $RANDOM --episodes 10  --env-name MiniGrid-Helper-8x8-v0 &