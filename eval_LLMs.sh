# python -m scripts.LLM_test  --gif test_GPT_boxes --episodes 1 --max-steps 8 --model text-davinci-003 --env-args size 7 --env-name SocialAI-ColorBoxesLLMCSParamEnv-v1 --in-context-path llm_data/in_context_color_boxes.txt
# python -m scripts.LLM_test  --gif test_GPT_asoc --episodes 1 --max-steps 8 --model text-ada-001 --env-args size 7  --env-name SocialAI-AsocialBoxInformationSeekingParamEnv-v1 --in-context-path llm_data/in_context_asocial_box.txt --feed-full-ep

# python -m scripts.LLM_test  --gif test_GPT_boxes --episodes 1 --max-steps 8 --model bloom_560m --env-args size 7 --env-name SocialAI-ColorBoxesLLMCSParamEnv-v1 --in-context-path llm_data/in_context_color_boxes.txt
# python -m scripts.LLM_test  --gif test_GPT_asoc --episodes 1 --max-steps 8 --model bloom_560m --env-args size 7  --env-name SocialAI-AsocialBoxInformationSeekingParamEnv-v1 --in-context-path llm_data/in_context_asocial_box.txt --feed-full-ep



### bloom 560m

## boxes
# hist
#python -m scripts.LLM_test --log llm_log/bloom_560m_boxes_hist --gif evaluation --episodes 20 --max-steps 10 --model bloom_560m --env-args size 7 --env-name SocialAI-ColorBoxesLLMCSParamEnv-v1 --in-context-path llm_data/in_context_color_boxes.txt --feed-full-ep --skip-check
# no hist
#python -m scripts.LLM_test --log llm_log/bloom_560m_boxes_no_hist --gif evaluation --episodes 20 --max-steps 10 --model bloom_560m --env-args size 7 --env-name SocialAI-ColorBoxesLLMCSParamEnv-v1 --in-context-path llm_data/in_context_color_boxes.txt --skip-check
#
## asocial
# hist
#python -m scripts.LLM_test  --log llm_log/bloom_560m_asocial_hist --gif evaluation --episodes 20 --max-steps 10 --model bloom_560m --env-args size 7  --env-name SocialAI-AsocialBoxInformationSeekingParamEnv-v1 --in-context-path llm_data/in_context_asocial_box.txt --feed-full-ep --skip-check
# no hist
#python -m scripts.LLM_test --log llm_log/bloom_560m_asocial_no_hist --gif evaluation --episodes 20 --max-steps 10 --model bloom_560m --env-args size 7  --env-name SocialAI-AsocialBoxInformationSeekingParamEnv-v1 --in-context-path llm_data/in_context_asocial_box.txt --skip-check

### gpt2_large

### boxes
## hist
#python -m scripts.LLM_test --log llm_log/gpt2_large_boxes_hist --gif evaluation --episodes 20 --max-steps 10 --model gpt2_large --env-args size 7 --env-name SocialAI-ColorBoxesLLMCSParamEnv-v1 --in-context-path llm_data/in_context_color_boxes.txt --feed-full-ep --skip-check
## no hist
#python -m scripts.LLM_test --log llm_log/gpt2_large_boxes_no_hist --gif evaluation --episodes 20 --max-steps 10 --model gpt2_large --env-args size 7 --env-name SocialAI-ColorBoxesLLMCSParamEnv-v1 --in-context-path llm_data/in_context_color_boxes.txt --skip-check
##
### asocial
## hist
#python -m scripts.LLM_test  --log llm_log/gpt2_large_asocial_hist --gif evaluation --episodes 20 --max-steps 10 --model gpt2_large --env-args size 7  --env-name SocialAI-AsocialBoxInformationSeekingParamEnv-v1 --in-context-path llm_data/in_context_asocial_box.txt --feed-full-ep --skip-check
## no hist
#python -m scripts.LLM_test --log llm_log/gpt2_large_asocial_no_hist --gif evaluation --episodes 20 --max-steps 10 --model gpt2_large --env-args size 7  --env-name SocialAI-AsocialBoxInformationSeekingParamEnv-v1 --in-context-path llm_data/in_context_asocial_box.txt --skip-check

### text-ada-001
# asoc hist
#python -m scripts.LLM_test  --log llm_log/ada_asocial_hist --gif evaluation --episodes 4 --max-steps 7 --model text-ada-001 --env-args size 7  --env-name SocialAI-AsocialBoxInformationSeekingParamEnv-v1 --in-context-path llm_data/in_context_asocial_box.txt --feed-full-ep
# boxes no hist
#python -m scripts.LLM_test --log llm_log/ada_boxes_no_hist --gif evaluation --episodes 4 --max-steps 7 --model text-ada-001 --env-args size 7 --env-name SocialAI-ColorBoxesLLMCSParamEnv-v1 --in-context-path llm_data/in_context_color_boxes.txt

### text-davinci-003
# no hist
#python -m scripts.LLM_test --log llm_log/davinci_boxes_no_hist --gif evaluation --episodes 4 --max-steps 5 --model text-davinci-003 --env-args size 7 --env-name SocialAI-ColorBoxesLLMCSParamEnv-v1 --in-context-path llm_data/in_context_color_boxes.txt


# TEST
python -m scripts.LLM_test --log llm_log/TEST_LOG --gif TEST_evaluation --episodes 1 --max-steps 7 --model text-ada-001 --env-args size 7 --env-name SocialAI-ColorBoxesLLMCSParamEnv-v1 --in-context-path llm_data/in_context_color_boxes.txt
